<?php 

$base_url = 'http://localhost/design-pattern/';
$dir_project = '/design-pattern';

// URI Configuration
function request_url($dir){
   $url = $_SERVER['REQUEST_URI'];
   $url = str_replace($dir, '', $url);
   $url = explode('?', $url);
   return $url[0];
}